package gui;

import javax.swing.*;

public class Vista {
    private JTabbedPane tabbedPane1;
    private JPanel panel1;
    private JPanel JPanelEquipo;
    private JPanel JPanelDirectiva;
    private JPanel JPanelJugador;
    private JTextField textField1;
    private JTextField textField2;
    private JRadioButton escolarRadioButton;
    private JRadioButton federadoRadioButton;
    private JTextField textField3;
    private JTable table1;
    private JButton nuevoEquipoButton;
    private JButton eliminarEquipoButton;
    private JButton buscarPorCategoriaButton;
    private JTextField textField4;
    private JTable table2;
    private JComboBox comboBox1;
    private JComboBox comboBox2;
    private JTextField textField5;
    private JTextField textField6;
    private JTextField textField7;
    private JButton nuevoButton;
    private JButton eliminarButton;
    private JButton buscarPorPosicionButton;
    private JTable table3;
    private JTextField textField8;
    private JPanel JPanelPartidos;
    private JTable table4;
    private JButton nuevoPartidoButton;
    private JTextField textField9;
    private JTextField textField10;
    private JTextField textField11;
    private JTextField textField12;
    private JComboBox comboBox3;
    private JButton buscarPorCentroButton;
    private JTable table5;
}
